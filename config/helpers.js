const path = require('path');

const projectRootPath = path.resolve(__dirname, '..');
const sourceRootPath = path.resolve(projectRootPath, 'src/main/javascript');
const targetRootPath = path.resolve(projectRootPath, 'target/classes');

function resolve(rootPath, args) {
  const argsArr = Array.prototype.slice.call(args, 0);
  return path.join(...[rootPath].concat(argsArr));
}

function root(...args) {
  return resolve(projectRootPath, args);
}

function sourceRoot(...args) {
  return resolve(sourceRootPath, args);
}

function targetRoot(...args) {
  return resolve(targetRootPath, args);
}


exports.root = root;
exports.sourceRoot = sourceRoot;
exports.targetRoot = targetRoot;
