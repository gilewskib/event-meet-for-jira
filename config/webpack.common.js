const helpers = require('./helpers');

const ManifestPlugin = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {
  entry: {
    pluginConfiguration: ['babel-polyfill', helpers.sourceRoot('app', 'pluginConfiguration', 'pluginConfiguration.root.tsx')],
    publicGroupsConfig: ['babel-polyfill', helpers.sourceRoot('app', 'publicGroupsConfig', 'publicGroupsConfig.root.tsx')],
  },

  resolve: {
    extensions: ['.jsx', '.js', '.tsx', '.ts'],
  },

  output: {
    path: helpers.targetRoot('app'),
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(ts|tsx)?$/,
        include: helpers.sourceRoot(),
        loader: 'eslint-loader',
        options: {
          failOnError: true,
        },
      },
      {
        test: /\.(ts|tsx)?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.jsx?$/,
        include: helpers.sourceRoot(),
        loader: 'babel-loader',
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|jpeg|gif|woff)$/,
        include: helpers.sourceRoot(),
        loader: 'url-loader?limit=100000',
      },
      {
        test: /\.svg$/,
        include: helpers.sourceRoot(),
        loader: 'url-loader?limit=10000&mimetype=image/svg+xml',
      },
      {
        test: /\.jpg$/,
        include: helpers.sourceRoot(),
        loader: 'file-loader',
      },
      {
        test: /\.json$/,
        include: helpers.sourceRoot(),
        loader: 'json-loader',
      },
    ],
  },
  optimization: {
    splitChunks: {
      chunks: 'initial',
      name: 'webpackCommonImports',
      filename: 'webpackCommonImports.js',
    },
  },
  plugins: [
    new ManifestPlugin({
      fileName: 'build-manifest.json',
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
};
