const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const baseConfig = require('./webpack.common.js');

module.exports = webpackMerge(baseConfig, {
  mode: 'development',
  devtool: 'source-map',
  output: {
    filename: '[name].js',
  },
});
