const helpers = require('./helpers');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const baseConfig = require('./webpack.common.js');

const stripLoader = require('strip-loader');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = webpackMerge(baseConfig, {
  output: {
    filename: '[name].js',
  },

  module: {
    rules: [
      {
        test: [/\.jsx?$/],
        include: helpers.sourceRoot(),
        loader: stripLoader.loader('console.log', 'console.debug'),
      },
    ],
  },
});
