export default interface Project {
  name: string
  key: string
  description: string
}
