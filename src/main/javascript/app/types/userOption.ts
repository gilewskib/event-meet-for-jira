export default interface UserOption {
  name: string
  key: string
  displayName: string
}
