export default interface Group {
  id?: number,
  name: string,
  type: string,
  ownerId?: number,
  members: number[]
}
