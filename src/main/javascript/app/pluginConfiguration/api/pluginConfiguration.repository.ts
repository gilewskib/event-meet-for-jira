import axios, { AxiosResponse } from 'axios';
import Project from '../../types/project';

const pluginConfigurationContextPath = `${AJS.contextPath()}/rest/event-meet/1.0/plugin-configuration`;

export const createProject = (project: Project): Promise<void> => axios
  .post(pluginConfigurationContextPath, project)
  .then((response: AxiosResponse<void>) => response.data);

export const aaa = 'aaa';
