import React from 'react';
import { connect } from 'react-redux';
import Project from '../../../types/project';
import CreateProjectDialog from '../../components/dialogs/createProjectDialog.component';
import * as StoreManager from '../../store/pluginConfiguration.store.manager';
import PluginConfigurationStore from '../../store/pluginConfiguration.store.types';
import * as PluginConfigurationRepository from '../../api/pluginConfiguration.repository';

interface Props {
  visible: boolean
}
interface State {
  projectData: Project
}

const defaultState: State = {
  projectData: {
    name: '',
    key: '',
    description: '',
  },
};

class CreateProjectDialogContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = defaultState;
  }

  onClose() {
    StoreManager.setPluginConfigurationDialogVisible(false);
    this.setState(defaultState);
  }

  onCreate() {
    PluginConfigurationRepository.createProject(this.state.projectData).then(() => this.onClose());
  }

  render() {
    return (
      <CreateProjectDialog
        data={this.state.projectData}
        onChange={(projectData: Project) => this.setState({ projectData })}
        onClose={() => this.onClose()}
        onCreate={() => this.onCreate()}
        visible={this.props.visible}
      />
    );
  }
}
const mapStateToProps = (store: PluginConfigurationStore) => ({
  visible: store.createProjectDialogVisible,
});
export default connect(mapStateToProps)(CreateProjectDialogContainer);
