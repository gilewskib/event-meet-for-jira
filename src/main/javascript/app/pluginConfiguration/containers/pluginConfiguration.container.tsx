import * as React from 'react';
import Button from '@atlaskit/button';
import * as StoreManager from '../store/pluginConfiguration.store.manager';
import CreateProjectDialogContainer from './dialogs/createProjectDialog.container';

interface State {
  sample: string
}
export default class PluginConfigurationContainer extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      sample: 'STAN',
    };
  }

  onClick() {
    StoreManager.setPluginConfigurationDialogVisible(true);
    this.setState((previousState) => ({ ...previousState, sample: 'aaa' }));
  }

  render() {
    return (
      <>
        <Button
          onClick={() => this.onClick()}
        >
          {AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project')}
        </Button>
        <CreateProjectDialogContainer />
      </>
    );
  }
}
