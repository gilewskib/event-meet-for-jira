import { createStore, Action, Store } from 'redux';
import PluginConfigurationStore from './pluginConfiguration.store.types';
import pluginConfigurationReducer from './pluginConfiguration.reducer';

const initialState: PluginConfigurationStore = {
  createProjectDialogVisible: false,
};

const store: Store<PluginConfigurationStore, Action> = createStore<PluginConfigurationStore, any, any, any>(pluginConfigurationReducer, initialState);
export default store;
