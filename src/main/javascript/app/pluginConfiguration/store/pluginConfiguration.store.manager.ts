import store from './pluginConfiguration.store';
import { ActionType } from './pluginConfiguration.store.types';

export function setPluginConfigurationDialogVisible(createProjectDialogVisible: boolean) {
  store.dispatch({
    type: ActionType.SET_CREATE_PROJECT_DIALOG_VISIBLE,
    createProjectDialogVisible,
  });
}
export function aaa() {

}
