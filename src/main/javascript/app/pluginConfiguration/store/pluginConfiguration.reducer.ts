import PluginConfigurationStore, { ActionData, ActionType } from './pluginConfiguration.store.types';

export default function pluginConfigurationReducer(state: PluginConfigurationStore, action: ActionData) {
  switch (action.type) {
    case ActionType.SET_CREATE_PROJECT_DIALOG_VISIBLE: {
      return {
        ...state,
        createProjectDialogVisible: action.createProjectDialogVisible,
      };
    }
    default: {
      return state;
    }
  }
}
