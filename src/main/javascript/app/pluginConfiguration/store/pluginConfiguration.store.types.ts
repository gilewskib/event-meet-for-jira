export default interface PluginConfigurationStore {
  createProjectDialogVisible: boolean
}

export enum ActionType {
  SET_CREATE_PROJECT_DIALOG_VISIBLE = 'SET_CREATE_PROJECT_DIALOG_VISIBLE'
}

export interface ActionData {
  type: ActionType,
  [x: string]: any,
}
