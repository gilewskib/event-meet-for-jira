import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import PluginConfigurationContainer from './containers/pluginConfiguration.container';
import store from './store/pluginConfiguration.store';

AJS.toInit(() => {
  ReactDOM.render(
    <Provider store={store}>
      <PluginConfigurationContainer />
    </Provider>,
    document.getElementById('root'),
  );
});
