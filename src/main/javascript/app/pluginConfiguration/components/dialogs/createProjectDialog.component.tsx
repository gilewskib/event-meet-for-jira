import React from 'react';
import TextField from '@atlaskit/textfield';
import ModalDialog, { ModalTransition } from '@atlaskit/modal-dialog';
import Project from '../../../types/project';

interface Props {
  data: Project
  onChange: (project: Project) => void,
  onClose: () => void,
  onCreate: () => void,
  visible: boolean,
}

const CreateProjectDialog = (props: Props) => {
  const onProjectNameChange = (projectName: string) => {
    props.onChange({ ...props.data, name: projectName });
  };
  const onProjectKeyChange = (projectKey: string) => {
    props.onChange({ ...props.data, key: projectKey });
  };
  const onProjectDescriptionChange = (projectDescription: string) => {
    props.onChange({ ...props.data, description: projectDescription });
  };
  const actions = [{
    text: AJS.I18n.getText('pl.com.tt.eventmeet.create'),
    onClick: props.onCreate,
  }, {
    text: AJS.I18n.getText('pl.com.tt.eventmeet.close'),
    onClick: props.onClose,
  },
  ];
  return (
    <ModalTransition>
      {props.visible && (
        <ModalDialog
          heading={AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project')}
          actions={actions}
          onClose={props.onClose}
        >
          <label htmlFor="project-name-input">{AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project.project-name')}</label>
          <TextField
            name="project-name-input"
            id="project-name-input"
            label={AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project.project-name')}
            onChange={(element) => onProjectNameChange(element.currentTarget.value)}
            value={props.data.name}
          />
          <label htmlFor="project-key-input">{AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project.project-key')}</label>
          <TextField
            name="project-key-input"
            id="project-key-input"
            label={AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project.project-key')}
            onChange={(element) => onProjectKeyChange(element.currentTarget.value)}
            value={props.data.key}
          />
          <label htmlFor="project-description-input">{AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project.project-description')}</label>
          <TextField
            name="project-description-input"
            id="project-description-input"
            label={AJS.I18n.getText('pl.com.tt.eventmeet.actions.create-project.project-description')}
            onChange={(element) => onProjectDescriptionChange(element.currentTarget.value)}
            value={props.data.description}
          />
        </ModalDialog>
      )}
    </ModalTransition>
  );
};
export default CreateProjectDialog;
