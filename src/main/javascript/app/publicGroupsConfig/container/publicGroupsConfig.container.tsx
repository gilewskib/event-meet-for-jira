import * as React from 'react';
import Group from '../../types/group';
import PublicGroupsConfig from '../component/publicGroupsConfig.component';
import { findUsers, getPublicGroups } from '../api/groupRepository';
import AddGroupDialog from '../component/addGroupDialog.component';

interface State {
  availableGroups: Group[]
  isLoading: boolean
  addDialogOpen: boolean
  selectedGroup: Group
  usersOptions: any[]
}

export default class PublicGroupsConfigContainer extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      availableGroups: [],
      isLoading: true,
      addDialogOpen: false,
      selectedGroup: {
        name: '',
        members: [],
        type: 'PUBLIC',
      },
      usersOptions: [],
    };
  }


  componentDidMount(): void {
    getPublicGroups().then((availableGroups) => {
      this.setState({
        availableGroups,
        isLoading: false,
      });
    });
    findUsers().then((users) => {
      this.setState({
        usersOptions: users,
      });
    });
  }

  onChange(event: any) {
    const { value = '' } = event.currentTarget;
    const { members } = this.state.selectedGroup;
    const selectedGroup = {
      members,
      name: value,
      type: 'PUBLIC',
    };
    this.setState({
      selectedGroup,
    });
  }

  onFormSubmit(data: any) {
    console.log(data);
  }

  setSelectedGroup(id: number) {
    const { availableGroups } = this.state;
    const selectedGroup = availableGroups.filter((groupElement) => groupElement.id === id)[0];
    this.setState({
      selectedGroup,
    });
  }

  setSelectedUsers(users: any = []) {
    const { selectedGroup } = this.state;
    const userIds = users.map((user: any) => user.id);
    this.setState({
      selectedGroup: {
        ...selectedGroup,
        members: userIds,
      },
    });
  }

  toggleDialog(show: boolean) {
    this.setState(
      { addDialogOpen: show },
    );
  }

  render() {
    return (
      <>
        {this.state.addDialogOpen
        && (
          <AddGroupDialog
            onChange={(event: any) => this.onChange(event)}
            group={this.state.selectedGroup}
            onClose={() => this.toggleDialog(false)}
            onFormSubmit={(data: any) => this.onFormSubmit(data)}
            onUsersPickerChange={(users: any) => this.setSelectedUsers(users)}
            usersPromptList={this.state.usersOptions}
          />
        )}
        <PublicGroupsConfig
          availableGroups={this.state.availableGroups}
          isLoading={this.state.isLoading}
          onAddClick={() => this.toggleDialog(true)}
          onGroupClick={(id: any) => this.setSelectedGroup(id)}
        />
      </>
    );
  }
}
