import axios, { AxiosResponse } from 'axios';
import Group from '../../types/group';
import UserOption from '../../types/userOption';

const requestPath = `${AJS.contextPath()}/rest/event-meet/1.0/group`;
const jiraRequestPath = `${AJS.contextPath()}/rest/api/2`;
const wildcardParamValue = '.';

export const getPublicGroups = (): Promise<Group[]> => axios
  .get(`${requestPath}`, {
    params: {
      type: 'PUBLIC',
    },
  })
  .then((response: AxiosResponse<Group[]>) => response.data);

export const findUsers = (query?: string): Promise<UserOption[]> => axios
  .get(`${jiraRequestPath}/user/picker`, {
    params: {
      query: query || wildcardParamValue,
      showAvatar: true,
    },
  })
  .then((response) => response.data.users);
