import * as React from 'react';
import * as ReactDOM from 'react-dom';
import PublicGroupsConfigContainer from './container/publicGroupsConfig.container';

AJS.toInit(() => {
  ReactDOM.render(
    <PublicGroupsConfigContainer />,
    document.getElementById('root'),
  );
});
