import React from 'react';
import DynamicTable from '@atlaskit/dynamic-table';
import AddIcon from '@atlaskit/icon/glyph/add';
import Button from '@atlaskit/button';
import PageHeader from '@atlaskit/page-header';
import Group from '../../types/group';


interface Props {
  availableGroups: Group[],
  isLoading: boolean,
  onGroupClick: (id: number) => void
  onAddClick: () => void
}

const PublicGroupsConfig = (props: Props) => {
  const head = {
    cells: [
      {
        key: 1,
        content: 'First',
        isSortable: true,
      },
      {
        key: 'edit',
        shouldTruncate: true,
      },
    ],

  };
  const { availableGroups } = props;

  const actionsContent = (
    <Button
      id="button-open-import-dialog"
      // @ts-ignore
      iconBefore={<AddIcon />}
      onClick={props.onAddClick}
    >
      Add
    </Button>
  );


  const rows = availableGroups.map((group) => ({
    cells: [
      {
        key: 1,
        content: group.name,
      },
      {
        key: 'edit',
        content: (
          <div style={{ float: 'right' }}>
            <Button>
            Edit
            </Button>
          </div>
        ),
      },

    ],
  }));
  return (
    <>
      <PageHeader
        actions={actionsContent}
      >
        Global groups configuration
      </PageHeader>
      <DynamicTable
        head={head}
        rows={rows}
        rowsPerPage={10}
        defaultPage={1}
        loadingSpinnerSize="large"
        isLoading={props.isLoading}
        isFixedSize
        defaultSortKey="term"
        defaultSortOrder="ASC"
      />
    </>
  );
};

export default PublicGroupsConfig;
