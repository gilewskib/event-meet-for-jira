import React from 'react';
import Button, { ButtonGroup } from '@atlaskit/button';
import Form from '@atlaskit/form';
import Textfield from '@atlaskit/textfield';
import ModalDialog, { ModalFooter, ModalTransition } from '@atlaskit/modal-dialog';
import { FooterProps } from '@atlaskit/modal-dialog/dist/esm/components/Footer';
import Group from '../../types/group';
import UserMultiPicker from './userMultiPicker.component';


interface Props {
  onClose: () => void,
  onFormSubmit: (data: any) => void,
  group: Group,
  onChange: (event: any) => void,
  usersInputLoading: boolean,
  usersPromptList: any,
  onUsersPickerChange: (users: any[]) => void,
}


interface ContainerProps {
  children: React.ReactNode;
  className?: string;
}

const footer = (props: FooterProps) => (
  <ModalFooter>
    <span />
    <ButtonGroup>
      <Button appearance="primary" type="submit">
        Submit
      </Button>
      <Button onClick={props.onClose}>
        Close
      </Button>
    </ButtonGroup>
  </ModalFooter>
);

const AddGroupDialog = (props: Props) => (
  <ModalTransition>
    <ModalDialog
      heading="Select group properties"
      onClose={props.onClose}
      components={{
        Container: ({ children, className }: ContainerProps) => (
          <div id="public-groups-wrapper" style={{ height: '500 px' }}>
            <Form onSubmit={props.onFormSubmit}>
              {({ formProps }) => (
                <form {...formProps} className={`${className} public-group-form`}>
                  {children}
                </form>
              )}
            </Form>
          </div>
        ),
        Footer: footer,
      }}
    >
      <Textfield
        width="large"
        placeholder="Enter group name"
        value={props.group.name}
        onChange={props.onChange}
      />
      <UserMultiPicker
        formId="group-form-id"
        isLoading={props.usersInputLoading}
        usersPromptList={props.usersPromptList}
        selectedUsers={props.group.members}
        onChange={(input: any) => props.onUsersPickerChange(input)}
      />
    </ModalDialog>
  </ModalTransition>

);

AddGroupDialog.defaultProps = {
  rolesOptions: [],
  usersPromptList: [],
  groupsPromptList: [],
  projectRolesLoading: false,
  usersInputLoading: false,
  groupsInputLoading: false,
  isLoading: false,
};

export default AddGroupDialog;
