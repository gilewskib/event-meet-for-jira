import React from 'react';
import PropTypes from 'prop-types';
import Select, { components } from '@atlaskit/select';
import { Field } from '@atlaskit/form';


class UserMultiPicker extends React.Component {
  getOption(props) {
    return (
      <components.Option {...props}>
        <span className="aui-avatar aui-avatar-xsmall tsp-user-multi-picker-user-avatar">
          <span className="aui-avatar-inner">
            <img src={props.data.avatarUrl} alt="avatar" />
          </span>
        </span>
        <span className="tsp-user-text-name-option">
          {props.data.label}
        </span>
      </components.Option>
    );
  }

  getSingleValue(props) {
    return (
      <components.SingleValue {...props}>
        <span className="aui-avatar aui-avatar-xsmall tsp-user-multi-picker-user-avatar">
          <span className="aui-avatar-inner">
            <img src={props.data.avatarUrl} alt="avatar" />
          </span>
        </span>
        <span className="tsp-user-text-name-option">
          {props.data.label}
        </span>
      </components.SingleValue>
    );
  }

  render() {
    const options = this.props.usersPromptList.map((user) => (
      { ...user, label: user.displayName, value: user.key }));
    const values = this.props.selectedUsers.map((user) => (
      { ...user, label: user.displayName, value: user.key }));
    return (
      <Field name="users" label="Pick users">
        {({ fieldProps }) => (
          <>
            <Select
              {...fieldProps}
              className="tsp-user-multi-picker"
              isMulti
              isSearchable
              isLoading={this.props.isLoading}
              placeholder="Select users"
              components={{ Option: this.getOption, SingleValue: this.getSingleValue }}
              inputId={`${this.props.formId}-user-multi-picker`}
              options={options}
              onChange={(users) => this.props.onChange(users)}
              value={values}
              isDisabled={this.props.isDisabled}
            />
          </>
        )}
      </Field>
    );
  }
}

UserMultiPicker.propTypes = {
  formId: PropTypes.string.isRequired,
  usersPromptList: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  })),
  onChange: PropTypes.func.isRequired,
  selectedUsers: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
  })),
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
};
UserMultiPicker.defaultProps = {
  usersPromptList: [],
  selectedUsers: [],
  isLoading: false,
  isDisabled: false,
};
export default UserMultiPicker;
