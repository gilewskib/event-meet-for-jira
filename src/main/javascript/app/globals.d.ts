declare namespace AJS {
  function toInit(func: Function): void;

  function contextPath(): string;

  namespace I18n {
    function getText(key: string, ...params: any[]): string;
  }

}
