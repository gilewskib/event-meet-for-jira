package pl.com.tt.eventmeet.actions;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import webwork.action.Action;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Scanned
public class ConfigurePublicGroups extends JiraWebActionSupport {

    private final ProjectManager projectManager;

    @Getter
    @Setter
    private String projectKey;
    @Getter
    @Setter
    private Long projectId;

    @Autowired
    public ConfigurePublicGroups(@ComponentImport ProjectManager projectManager) {
        this.projectManager = projectManager;
    }

    @Override
    public String doDefault() {
        if (isEmpty(projectKey)) {
            getErrorMessages().add(getText("pl.com.tt.thescheduler.errors.project.key.empty"));
            return Action.ERROR;
        }
        Project project = projectManager.getProjectObjByKey(projectKey);

        projectId = project.getId();
        setSelectedProjectId(projectId);
        return Action.SUCCESS;
    }
}
