package pl.com.tt.eventmeet.actions;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import webwork.action.Action;

public class ConfigurePlugin extends JiraWebActionSupport {
    @Override
    public String doDefault() throws Exception {
        return Action.SUCCESS;
    }
}
