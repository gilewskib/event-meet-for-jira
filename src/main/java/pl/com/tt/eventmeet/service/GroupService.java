package pl.com.tt.eventmeet.service;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.tt.eventmeet.model.Group;
import pl.com.tt.eventmeet.model.GroupMember;
import pl.com.tt.eventmeet.model.ao.GroupEntity;
import pl.com.tt.eventmeet.model.ao.GroupMemberEntity;
import pl.com.tt.eventmeet.model.dao.GroupDao;
import pl.com.tt.eventmeet.model.dao.GroupMemberDao;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Scanned
public class GroupService {
    private GroupDao groupDao;
    private GroupMemberDao groupMemberDao;

    @Autowired
    public GroupService(GroupDao groupDao, GroupMemberDao groupMemberDao) {
        this.groupDao = groupDao;
        this.groupMemberDao = groupMemberDao;
    }

    public List<Group> readPublicGroups() {
        return groupDao.readPublicGroups().stream()
                .map(this::getGroup)
                .collect(Collectors.toList());
    }

    public Group readGroup(Integer id) {
        GroupEntity read = groupDao.read(id);
        return read != null ? getGroup(read) : null;
    }

    public Group getGroup(GroupEntity entity) {
        List<GroupMember> membres = Arrays.stream(entity.getGroupMembers())
                .map(groupMemberEntity -> {
                    GroupMember groupMember = new GroupMember();
                    groupMember.setUserId(groupMemberEntity.getUserId());
                    return groupMember;
                }).collect(Collectors.toList());
        return Group.builder()
                .id(entity.getID())
                .name(entity.getName())
                .type(entity.getType())
                .ownerId(entity.getOwnerId())
                .members(membres)
                .build();

    }

    public void saveGroup(Group group) {
        List<GroupMember> members = group.getMembers();
        GroupEntity oldGroup = groupDao.read(group.getId());


        if (oldGroup == null) {
            groupDao.create(group);
        } else {
            for (GroupMemberEntity member : oldGroup.getGroupMembers()) {
                groupMemberDao.delete(member);
            }
            groupDao.putGroup(group);
        }
        for (GroupMember member : members) {
            groupMemberDao.create(member, group.getId());
        }

    }
}

