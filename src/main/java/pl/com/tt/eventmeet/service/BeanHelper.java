package pl.com.tt.eventmeet.service;

import org.springframework.stereotype.Service;
import pl.com.tt.eventmeet.bean.GroupBean;
import pl.com.tt.eventmeet.model.Group;
import pl.com.tt.eventmeet.model.GroupMember;
import pl.com.tt.eventmeet.model.ao.GroupEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BeanHelper {

    public GroupBean getGroupBean(Group group) {
        List<Long> members;
        if (group.getMembers() != null && group.getMembers().size() > 0) {
            members = group.getMembers().stream()
                    .map(GroupMember::getUserId)
                    .collect(Collectors.toList());
        } else {
            members = new ArrayList<>();
        }
        return GroupBean.builder()
                .id(group.getId())
                .name(group.getName())
                .type(group.getType().toString())
                .ownerId(group.getOwnerId())
                .members(members)
                .build();
    }

    public Group getGroup(GroupBean bean) {
        int id = bean.getId();
        String name = bean.getName();
        Long ownerId = bean.getOwnerId();
        String type = bean.getType();
        List<GroupMember> members = bean.getMembers().stream()
                .map(userId -> {
                    GroupMember groupMember = new GroupMember();
                    groupMember.setUserId(userId);
                    return groupMember;
                }).collect(Collectors.toList());

        return Group.builder()
                .name(name)
                .id(id)
                .ownerId(ownerId)
                .type(GroupEntity.Type.valueOf(type))
                .members(members)
                .build();
    }
}
