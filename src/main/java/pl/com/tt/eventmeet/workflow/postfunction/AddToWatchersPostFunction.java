package pl.com.tt.eventmeet.workflow.postfunction;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.tt.eventmeet.configuration.ProjectConfigurationService;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@Scanned
public class AddToWatchersPostFunction extends AbstractJiraFunctionProvider implements WorkflowPluginFactory {
    private final PluginSettingsFactory pluginSettingsFactory;
    private final CustomFieldManager customFieldManager;
    private final WatcherManager watcherManager;

    @Autowired
    public AddToWatchersPostFunction(
            @ComponentImport PluginSettingsFactory pluginSettingsFactory,
            @ComponentImport CustomFieldManager customFieldManager,
            @ComponentImport WatcherManager watcherManager
            ) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.customFieldManager = customFieldManager;
        this.watcherManager = watcherManager;
    }

    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        String participantsCustomFieldId = (String) pluginSettingsFactory.createGlobalSettings()
                .get(ProjectConfigurationService.PARTICIPANT_USERS_CUSTOM_FIELD_ID_PROPERTY_KEY);
        if(participantsCustomFieldId == null) {
            throw new WorkflowException("Participants Custom Field Id Not Found");
        }
        CustomField participantsCustomField = customFieldManager.getCustomFieldObject(participantsCustomFieldId);
        if(participantsCustomField == null) {
            throw new WorkflowException("Participants Custom Field Not Found");
        }
        Issue issue = getIssue(transientVars);
        Collection<ApplicationUser> selectedUsers = (Collection<ApplicationUser>) issue.getCustomFieldValue(participantsCustomField);
        if(selectedUsers != null) {
            selectedUsers.forEach(user -> watcherManager.startWatching(user, issue));
        }
    }
    @Override
    public Map<String, ?> getDescriptorParams(Map<String,Object> formParams) {
        return Collections.emptyMap();
    }

    @Override
    public Map<String, ?> getVelocityParams(String resourceName, com.opensymphony.workflow.loader.AbstractDescriptor descriptor) {
        return Collections.emptyMap();
    }
}
