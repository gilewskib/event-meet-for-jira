package pl.com.tt.eventmeet.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@JsonAutoDetect
@AllArgsConstructor
public class GroupBean {
    private int id;
    private String name;
    private String type;
    private Long ownerId;
    private List<Long> members;
}
