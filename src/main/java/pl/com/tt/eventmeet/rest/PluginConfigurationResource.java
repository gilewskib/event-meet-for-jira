package pl.com.tt.eventmeet.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.tt.eventmeet.configuration.ProjectBean;
import pl.com.tt.eventmeet.configuration.ProjectConfigurationService;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Scanned
@Path("/plugin-configuration")
public class PluginConfigurationResource {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ProjectConfigurationService projectConfigurationService;

    @Autowired
    public PluginConfigurationResource (
            @ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
            ProjectConfigurationService projectConfigurationService
    ) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.projectConfigurationService = projectConfigurationService;
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createProject(ProjectBean projectBean) {
        projectConfigurationService.createProjectWithConfiguration(projectBean, jiraAuthenticationContext.getLoggedInUser());
        return Response.ok().build();
    }
}
