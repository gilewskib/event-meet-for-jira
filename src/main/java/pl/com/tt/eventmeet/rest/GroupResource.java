package pl.com.tt.eventmeet.rest;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.tt.eventmeet.bean.GroupBean;
import pl.com.tt.eventmeet.model.Group;
import pl.com.tt.eventmeet.model.ao.GroupEntity;
import pl.com.tt.eventmeet.service.BeanHelper;
import pl.com.tt.eventmeet.service.GroupService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/group")
@Scanned
public class GroupResource {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final BeanHelper beanHelper;
    private final GroupService groupService;

    @Autowired
    public GroupResource(
            @ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
            BeanHelper beanHelper, GroupService groupService) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.beanHelper = beanHelper;
        this.groupService = groupService;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPublicGroups(@QueryParam("type") GroupEntity.Type groupType) {
        List<GroupBean> beans = groupService.readPublicGroups().stream()
                .map(beanHelper::getGroupBean)
                .collect(Collectors.toList());

        return Response.ok(beans).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response putPublicGroup(GroupBean bean) {
        Group group = beanHelper.getGroup(bean);
        groupService.saveGroup(group);
        return Response.ok(bean).build();
    }
}
