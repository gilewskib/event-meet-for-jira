package pl.com.tt.eventmeet.field;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.AbstractMultiCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import pl.com.tt.eventmeet.model.Group;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

@Scanned
public class GroupField extends AbstractMultiCFType {
    public GroupField(@JiraImport CustomFieldValuePersister valuePersister,
                      @JiraImport GenericConfigManager genericConfigManager) {
        super(valuePersister, genericConfigManager);
    }

    @Override
    public String getStringFromSingularObject(Object o) {
        return null;
    }

    @Override
    public Object getSingularObjectFromString(String s) throws FieldValidationException {
        return null;
    }

    @Override
    public void validateFromParams(CustomFieldParams customFieldParams, ErrorCollection errorCollection, FieldConfig fieldConfig) {

    }

    @Override
    public void createValue(CustomField customField, Issue issue, @Nonnull Object o) {

    }

    @Override
    public void updateValue(CustomField customField, Issue issue, Object o) {

    }

    @Override
    public Object getValueFromCustomFieldParams(CustomFieldParams customFieldParams) throws FieldValidationException {
        return null;
    }

    @Override
    public Object getStringValueFromCustomFieldParams(CustomFieldParams customFieldParams) {
        return null;
    }

    @Override
    public void setDefaultValue(FieldConfig fieldConfig, Object o) {

    }

    @Nullable
    @Override
    public String getChangelogValue(CustomField customField, Object o) {
        return null;
    }

    @Override
    public Map getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
        HashMap<String, Object> result = new HashMap<>();
        List<Group> groups = getGroups();
        Map<Integer, String> optionsMap = groups.stream().collect(Collectors.toMap(Group::getId, Group::getName));
        result.put("results", optionsMap);
        return result;
    }

    private List<Group> getGroups() {
        List<Group> groups = new ArrayList<>();

        Group group1 = Group.builder()
                .name("group1")
                .id(11)
                .build();
        group1.setName("group1");
        group1.setId(111);
        Group group2 = Group.builder()
                .name("group2")
                .id(22)
                .build();

        groups.add(group2);
        groups.add(group1);
        return groups;
    }


    @Nullable
    @Override
    protected Comparator getTypeComparator() {
        return null;
    }

    @Nullable
    @Override
    protected Object convertTypeToDbValue(@Nullable Object o) {
        return null;
    }

    @Nullable
    @Override
    protected Object convertDbValueToType(@Nullable Object o) {
        return null;
    }

    @Nonnull
    @Override
    protected PersistenceFieldType getDatabaseType() {
        return null;
    }
}
