package pl.com.tt.eventmeet.model.ao;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;
import pl.com.tt.eventmeet.model.GroupMember;

@Preload
@Table("GROUP")
public interface GroupEntity extends Entity {
    String ID_COLUMN = "ID";
    String NAME_COLUMN = "NAME";
    String TYPE_COLUMN = "TYPE";
    String OWNER_ID_COLUMN = "OWNER_ID";

    @NotNull
    String getName();

    void setName(String name);

    @NotNull
    Type getType();

    void setType(Type type);

    Long getOwnerId();

    void setOwnerId(Long ownerId);

    @OneToMany
    GroupMemberEntity[] getGroupMembers();

    public enum Type {
        PUBLIC, PRIVATE
    }
}
