package pl.com.tt.eventmeet.model.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.ActiveObjectsException;
import net.java.ao.DBParam;
import net.java.ao.RawEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.com.tt.eventmeet.model.GroupMember;
import pl.com.tt.eventmeet.model.ao.GroupMemberEntity;

@Repository
public class GroupMemberDao {
    private final ActiveObjects activeObjects;

    @Autowired
    public GroupMemberDao(@ComponentImport ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }


    public GroupMemberEntity create(GroupMember groupMember, Integer groupId) {
        DBParam[] groupDBParams = new DBParam[]{
                new DBParam(GroupMemberEntity.USER_ID, groupMember.getUserId()),
                new DBParam(GroupMemberEntity.GROUP_ID, groupId),
        };
        return activeObjects.create(GroupMemberEntity.class, groupDBParams);
    }

    public GroupMemberEntity read(Integer id) {
        return activeObjects.get(GroupMemberEntity.class, id);
    }


    public void delete(Integer id) {
        GroupMemberEntity entity = read(id);
        if (entity == null) {
            throw new ActiveObjectsException("GroupMemberEntity with id " + id + "doesn't exist.");
        }
        delete(entity);
    }

    public void delete(GroupMemberEntity entity) {
        if (entity != null) {
            activeObjects.delete(entity);
        }
    }

    public void delete(RawEntity<?>[] res) {
        activeObjects.delete(res);
    }
}
