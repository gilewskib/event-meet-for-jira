package pl.com.tt.eventmeet.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.tt.eventmeet.model.ao.GroupEntity;

import java.util.List;

@Data
@Builder
public class Group {
    private int id;
    private String name;
    private GroupEntity.Type type;
    private Long ownerId;
    private List<GroupMember> members;
}
