package pl.com.tt.eventmeet.model.ao;

import net.java.ao.Entity;
import net.java.ao.OneToOne;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;
import pl.com.tt.eventmeet.model.Group;

@Preload
@Table("GROUP_MEMBER")
public interface GroupMemberEntity extends Entity {

    String USER_ID = "USER_ID";
    String GROUP_ID = "GROUP_ID";


    @NotNull
    long getUserId();

    void setUserId(long userId);

    @OneToOne
    GroupEntity getGroup();

    void setGroup(GroupEntity group);
}
