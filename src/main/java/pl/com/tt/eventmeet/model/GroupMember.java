package pl.com.tt.eventmeet.model;

import lombok.Data;

@Data
public class GroupMember {
    private long userId;
}
