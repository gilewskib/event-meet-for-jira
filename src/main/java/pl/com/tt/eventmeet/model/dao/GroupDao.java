package pl.com.tt.eventmeet.model.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.ActiveObjectsException;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.com.tt.eventmeet.model.Group;
import pl.com.tt.eventmeet.model.ao.GroupEntity;

import java.util.Arrays;
import java.util.List;

@Repository
public class GroupDao {
    private final ActiveObjects activeObjects;

    @Autowired
    public GroupDao(@ComponentImport ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    public GroupEntity create(Group group) {
        DBParam[] groupDBParams = new DBParam[]{
                new DBParam(GroupEntity.NAME_COLUMN, group.getName()),
                new DBParam(GroupEntity.TYPE_COLUMN, group.getType()),
                new DBParam(GroupEntity.OWNER_ID_COLUMN, group.getOwnerId()),
        };
        return activeObjects.create(GroupEntity.class, groupDBParams);
    }

    public GroupEntity read(Integer id) {
        return activeObjects.get(GroupEntity.class, id);
    }

    public List<GroupEntity> readPublicGroups() {
        return Arrays.asList(activeObjects.find(GroupEntity.class, Query.select()
                .where(GroupEntity.TYPE_COLUMN + " = ?", GroupEntity.Type.PUBLIC)));
    }

    public List<GroupEntity> readGroupsForUser(long userId) {
        return Arrays.asList(activeObjects.find(GroupEntity.class,
                Query.select()
                        .where(GroupEntity.TYPE_COLUMN + " = ? AND " + GroupEntity.OWNER_ID_COLUMN + " = ?",
                                GroupEntity.Type.PRIVATE,
                                userId
                        ))
        );
    }

    public void delete(Integer id) {
        GroupEntity entity = read(id);
        if (entity == null) {
            throw new ActiveObjectsException("GroupEntity with id " + id + "doesn't exist.");
        }
        delete(entity);
    }

    public void delete(GroupEntity entity) {
        if (entity != null) {
            activeObjects.delete(entity);
        }
    }

    public void putGroup(Group group) {
        GroupEntity entity = read(group.getId());
        entity.setName(group.getName());
        entity.setOwnerId(group.getOwnerId());
        entity.save();
    }
}
