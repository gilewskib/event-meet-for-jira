package pl.com.tt.eventmeet.configuration;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@Data
@JsonAutoDetect
public class ProjectBean {
    private String name;
    private String key;
    private String description;
}
