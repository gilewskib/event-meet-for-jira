package pl.com.tt.eventmeet.configuration;

import com.atlassian.jira.util.json.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class JsonStatusFileReader {
    InputStream statusesStream;
    JSONArray parsedArray;

    public JsonStatusFileReader(String path) {
        statusesStream = getClass().getResourceAsStream(path);
    }

    public List<StatusInfo> getStatusesInfoAsList() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.getJsonFactory().configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false);
        List<StatusInfo> mappedStatuses = null;
        try {
            mappedStatuses = mapper.readValue(statusesStream, StatusInfo.LIST_TYPE);
        } catch (IOException ex) {
            log.error("Error getting status", ex);
        }
        return mappedStatuses;
    }

    public Map<String, StatusInfo> getStatusesInfoAsMap() {
        Map<String, StatusInfo> map = new HashMap<>();
        for (StatusInfo status : getStatusesInfoAsList()) {
            map.put(status.getName(), status);
        }
        return map;
    }
}
