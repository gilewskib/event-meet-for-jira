package pl.com.tt.eventmeet.configuration;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.type.TypeReference;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusInfo implements Serializable {

    public static TypeReference<List<StatusInfo>> LIST_TYPE = new TypeReference<List<StatusInfo>>() {
    };

    private final String originalId;
    private final String name;
    private final String description;
    private final Long statusCategoryId;

    @JsonCreator
    public StatusInfo(
            @JsonProperty("originalId") String originalId,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("statusCategoryId") Long statusCategoryId) {
        this.originalId = originalId;
        this.name = name;
        this.description = description;
        this.statusCategoryId = statusCategoryId;
    }

    public Long getStatusCategoryId() {
        return statusCategoryId;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getOriginalId() {
        return originalId;
    }
}
