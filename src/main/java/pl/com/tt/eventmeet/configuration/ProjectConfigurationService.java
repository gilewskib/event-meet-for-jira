package pl.com.tt.eventmeet.configuration;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.config.StatusCategoryManager;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenFactory;
import com.atlassian.jira.issue.fields.screen.FieldScreenImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeItemImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeEntity;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeImpl;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.type.ProjectTypeKeys;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowScheme;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.opensymphony.workflow.InvalidWorkflowDescriptorException;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.loader.WorkflowLoader;
import lombok.extern.slf4j.Slf4j;
import org.ofbiz.core.entity.GenericEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Slf4j
public class ProjectConfigurationService {
    public static final String PARTICIPANT_USERS_CUSTOM_FIELD_ID_PROPERTY_KEY = "pl.com.tt.eventmeet.participant-users-custom-field-id";

    private final AvatarManager avatarManager;
    private final CustomFieldManager customFieldManager;
    private final FieldConfigSchemeManager fieldConfigSchemeManager;
    private final FieldManager fieldManager;
    private final FieldScreenFactory fieldScreenFactory;
    private final FieldScreenManager fieldScreenManager;
    private final FieldScreenSchemeManager fieldScreenSchemeManager;
    private final IssueTypeManager issueTypeManager;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private final PermissionSchemeService permissionSchemeService;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ProjectManager projectManager;
    private final StatusManager statusManager;
    private final StatusCategoryManager statusCategoryManager;
    private final WorkflowManager workflowManager;
    private final WorkflowSchemeManager workflowSchemeManager;

    private static final String MEETING_WORKFLOW_FILE_PATH = "/wfb/meeting_workflow.xml";
    private static final String ORDER_WORKFLOW_FILE_PATH = "/wfb/order_workflow.xml";
    private final String ICON_PATH = "/images/pluginIcon.png";
    private final String JIRA_STATUS_ID_KEY = "jira.status.id";
    //private static final String STATUS_FILE_PATH = "/wbf/test.xml";

    @Autowired
    public ProjectConfigurationService(
            @ComponentImport AvatarManager avatarManager,
            @ComponentImport CustomFieldManager customFieldManager,
            @ComponentImport FieldConfigSchemeManager fieldConfigSchemeManager,
            @ComponentImport FieldManager fieldManager,
            @ComponentImport FieldScreenFactory fieldScreenFactory,
            @ComponentImport FieldScreenManager fieldScreenManager,
            @ComponentImport FieldScreenSchemeManager fieldScreenSchemeManager,
            @ComponentImport IssueTypeManager issueTypeManager,
            @ComponentImport IssueTypeSchemeManager issueTypeSchemeManager,
            @ComponentImport IssueTypeScreenSchemeManager issueTypeScreenSchemeManager,
            @ComponentImport PermissionSchemeService permissionSchemeService,
            @ComponentImport PluginSettingsFactory pluginSettingsFactory,
            @ComponentImport ProjectManager projectManager,
            @ComponentImport StatusManager statusManager,
            @ComponentImport StatusCategoryManager statusCategoryManager,
            @ComponentImport WorkflowManager workflowManager,
            @ComponentImport WorkflowSchemeManager workflowSchemeManager) {
        this.avatarManager = avatarManager;
        this.customFieldManager = customFieldManager;
        this.fieldConfigSchemeManager = fieldConfigSchemeManager;
        this.fieldManager = fieldManager;
        this.fieldScreenFactory = fieldScreenFactory;
        this.fieldScreenManager = fieldScreenManager;
        this.fieldScreenSchemeManager = fieldScreenSchemeManager;
        this.issueTypeManager = issueTypeManager;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
        this.permissionSchemeService = permissionSchemeService;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.projectManager = projectManager;
        this.statusManager = statusManager;
        this.statusCategoryManager = statusCategoryManager;
        this.workflowManager = workflowManager;
        this.workflowSchemeManager = workflowSchemeManager;
    }

    public void createProjectWithConfiguration(ProjectBean projectBean, ApplicationUser user) {
        Project project = createProject(projectBean.getName(), projectBean.getKey(), projectBean.getDescription(), user);
        IssueType meetingIssueType = createIssueType("Meeting", "Used for classic meetings");
        IssueType orderIssueType = createIssueType("Order", "Used for ordering stuff");
        createIssueTypeScheme("Event Meet Issue Type Scheme", "Used for Event Meet for Jira plugin", meetingIssueType, orderIssueType, project);
        Map<String, Status> statuses = createStatuses();
        JiraWorkflow meetingWorkflow = importWorkflow("Event Meet Meeting Workflow", MEETING_WORKFLOW_FILE_PATH, user, statuses);
        JiraWorkflow orderWorkflow = importWorkflow("Event Meet Order Workflow", ORDER_WORKFLOW_FILE_PATH, user, statuses);

        createWorkflowScheme("Event Meet Workflow Scheme", "Workflow Scheme for Event Meet plugin",
                meetingWorkflow, orderWorkflow, meetingIssueType, orderIssueType, project);

        List<CustomField> customFields = createCustomFields(project);

        FieldScreen fieldScreen = createFieldScreen("Event Meet Default Screen", "Screen for default issue operations", customFields);
        FieldScreenScheme fieldScreenScheme = createFieldScreenScheme("Event Meet Screen Scheme", "", fieldScreen);
        IssueTypeScreenScheme issueTypeScreenScheme = createIssueTypeScreenScheme("Event Meet Issue Type Screen Scheme", "", fieldScreenScheme, project);

    }

    private Project createProject(String name, String key, String description, ApplicationUser lead) {
        ProjectCreationData data = new ProjectCreationData.Builder()
                .withLead(lead)
                .withName(name)
                .withKey(key)
                .withDescription(description)
                .withType(ProjectTypeKeys.BUSINESS)
                .build();

        Project project = projectManager.createProject(lead, data);
        permissionSchemeService.assignPermissionSchemeToProject(lead, 0L, project.getId());
        return project;
    }

    private IssueType createIssueType(String issueTypeName, String issueTypeDescription) {
        Avatar defaultIssueTypeAvatar = avatarManager.getDefaultAvatar(IconType.ISSUE_TYPE_ICON_TYPE);
        return issueTypeManager.createIssueType(issueTypeName, issueTypeDescription, defaultIssueTypeAvatar.getId());
    }

    private FieldConfigScheme createIssueTypeScheme(String issueTypeSchemeName, String issueTypeSchemeDescription, IssueType meetingIssueType, IssueType orderIssueType, Project project) {
        List<String> issueTypeIds = Stream.of(meetingIssueType.getId(), orderIssueType.getId()).collect(Collectors.toList());
        FieldConfigScheme issueTypeScheme = issueTypeSchemeManager.create(issueTypeSchemeName, issueTypeSchemeDescription, issueTypeIds);
        Long[] ids = {project.getId()};
        List contexts = CustomFieldUtils.buildJiraIssueContexts(false, ids, projectManager);
        fieldConfigSchemeManager.updateFieldConfigScheme(issueTypeScheme, contexts, fieldManager.getConfigurableField(IssueFieldConstants.ISSUE_TYPE));
        return issueTypeScheme;
    }

    private JiraWorkflow importWorkflow(String name, String path, ApplicationUser user, Map<String, Status> statusMap) {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(path);
        try {
            WorkflowDescriptor workflowDescriptor = WorkflowLoader.load(inputStream, false);
            List<StepDescriptor> steps = workflowDescriptor.getSteps();
            steps.forEach(stepDescriptor -> {
                Status status = statusMap.get(stepDescriptor.getName());
                Map metaAttributes = stepDescriptor.getMetaAttributes();
                metaAttributes.put(JIRA_STATUS_ID_KEY, status.getId());
            });
            ConfigurableJiraWorkflow configurableWorkflow = new ConfigurableJiraWorkflow(name, workflowManager);
            configurableWorkflow.setDescriptor(workflowDescriptor);
            workflowManager.createWorkflow(user, configurableWorkflow);
            return configurableWorkflow;
        } catch (SAXException | IOException | InvalidWorkflowDescriptorException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private WorkflowScheme createWorkflowScheme(String name, String description, JiraWorkflow meetingWorkflow, JiraWorkflow orderWorkflow,
                                                IssueType meetingIssueType, IssueType orderIssueType, Project project) {
        AssignableWorkflowScheme workflowScheme = workflowSchemeManager.assignableBuilder()
                .setName(name)
                .setDescription(description)
                .setDefaultWorkflow(meetingWorkflow.getName())
                .setMapping(meetingIssueType.getId(), meetingWorkflow.getName())
                .setMapping(orderIssueType.getId(), orderWorkflow.getName())
                .build();
        workflowScheme = workflowSchemeManager.createScheme(workflowScheme);
        Scheme scheme = workflowSchemeManager.getSchemeObject(workflowScheme.getId());
        workflowSchemeManager.addSchemeToProject(project, scheme);
        return workflowScheme;
    }

    private Map<String, Status> createStatuses() {
        List<StatusCategory> statusCategories = statusCategoryManager.getStatusCategories();
        StatusCategory todoCategory = statusCategories.stream().filter(statusCategory -> statusCategory.getKey().equals(StatusCategory.TO_DO)).findFirst().get();
        StatusCategory inprogressCategory = statusCategories.stream().filter(statusCategory -> statusCategory.getKey().equals(StatusCategory.IN_PROGRESS)).findFirst().get();
        StatusCategory doneCategory = statusCategories.stream().filter(statusCategory -> statusCategory.getKey().equals(StatusCategory.COMPLETE)).findFirst().get();

        Map<String, Status> statusMap = new HashMap<>();
        statusMap.put("Proposed", statusManager.createStatus("Proposed", "", ICON_PATH, todoCategory));
        statusMap.put("Order in progress", statusManager.createStatus("Order in progress", "", ICON_PATH, inprogressCategory));
        statusMap.put("Accepted", statusManager.createStatus("Accepted", "", ICON_PATH, doneCategory));
        statusMap.put("Completed", statusManager.createStatus("Completed", "", ICON_PATH, doneCategory));
        statusMap.put("Rejected", statusManager.createStatus("Rejected", "", ICON_PATH, doneCategory));
        return statusMap;
    }

    private List<CustomField> createCustomFields(Project project) {
        List<CustomField> customFields = new LinkedList<>();
        customFields.add(createCustomField("Start Date", "Start date of event",
                "com.atlassian.jira.plugin.system.customfieldtypes:datetime", project));
        CustomField participantsCustomField = createCustomField("Participants", "Participants of event",
                "com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker", project);
        pluginSettingsFactory.createGlobalSettings().put(PARTICIPANT_USERS_CUSTOM_FIELD_ID_PROPERTY_KEY, participantsCustomField.getId());
        customFields.add(participantsCustomField);
        return customFields;
    }

    private CustomField createCustomField(String name, String description, String customFieldType, Project project) {
        CustomFieldType cft = customFieldManager.getCustomFieldType(customFieldType);
        CustomFieldSearcher cfs = customFieldManager.getCustomFieldSearcher(customFieldType);
        List<IssueType> list_ist = new LinkedList<>();
        IssueType issueGlobal = issueTypeManager.getIssueType("-1");
        list_ist.add(issueGlobal);
        Long[] projectIds = { project.getId() };
        List<JiraContextNode> jiraContextNodeList = CustomFieldUtils.buildJiraIssueContexts(false, projectIds,
                projectManager);
        try {
            return customFieldManager.createCustomField(name, description, cft, cfs, jiraContextNodeList, list_ist);
        } catch (GenericEntityException ex) {
            log.error("Error while creating custom field", ex);
            return null;
        }
    }

    private FieldScreen createFieldScreen(String name, String description, List<CustomField> customFields) {
        FieldScreen fieldScreen = new FieldScreenImpl(fieldScreenManager);
        fieldScreen.setName(name);
        fieldScreen.setDescription(description);
        fieldScreen.store();
        FieldScreenTab fieldScreenTab = fieldScreen.addTab("Default");
        fieldScreenTab.addFieldScreenLayoutItem("summary");
        fieldScreenTab.addFieldScreenLayoutItem("description");
        customFields.forEach(customField -> fieldScreenTab.addFieldScreenLayoutItem(customField.getId()));
        return fieldScreen;
    }

    private FieldScreenScheme createFieldScreenScheme(String name, String description, FieldScreen fieldScreen) {
        FieldScreenScheme fieldScreenScheme = new FieldScreenSchemeImpl(fieldScreenSchemeManager);
        fieldScreenScheme.setName(name);
        fieldScreenScheme.setDescription(description);
        fieldScreenScheme.store();
        FieldScreenSchemeItem mySchemeItem = new FieldScreenSchemeItemImpl(fieldScreenSchemeManager, fieldScreenManager);
        //mySchemeItem.setIssueOperation(issueOperation); // or: EDIT_ISSUE_OPERATION, VIEW_ISSUE_OPERATION
        mySchemeItem.setFieldScreen(fieldScreen);
        fieldScreenScheme.addFieldScreenSchemeItem(mySchemeItem);
        return fieldScreenScheme;
    }

    private IssueTypeScreenScheme createIssueTypeScreenScheme(String name, String description, FieldScreenScheme fieldScreenScheme, Project project) {
        IssueTypeScreenScheme issueTypeScreenScheme = new IssueTypeScreenSchemeImpl(issueTypeScreenSchemeManager, null);
        issueTypeScreenScheme.setName(name);
        issueTypeScreenScheme.setDescription(description);
        issueTypeScreenScheme.store();

        IssueTypeScreenSchemeEntity issueTypeScreenSchemeEntity = fieldScreenFactory.createIssueTypeScreenSchemeEntity();
        issueTypeScreenSchemeEntity.setIssueTypeId(null);
        issueTypeScreenSchemeEntity.setFieldScreenScheme(fieldScreenScheme);

        issueTypeScreenScheme.addEntity(issueTypeScreenSchemeEntity);

        issueTypeScreenSchemeManager.addSchemeAssociation(project, issueTypeScreenScheme);

        return issueTypeScreenScheme;
    }

    /*private List<Status> generateSteps(List<StepDescriptor> steps, Collection<Status> existingStatuses,
                                       Map<String, StatusInfo> statusInfoMap) {
        List<Status> statuses = new LinkedList<>();
        boolean isExist = false;
        for (StepDescriptor sd : steps) {
            for (Status existingStatus : existingStatuses) {
                if (existingStatus.getName().trim().equalsIgnoreCase(sd.getName().trim())) {
                    isExist = true;
                    addStatusToStepDescriptor(sd, existingStatus);
                    statuses.add(existingStatus);
                }
            }
            if (isExist == false) {
                StatusInfo statusInfo = statusInfoMap.get(sd.getName());
                Status status = statusManager.createStatus(statusInfo.getName(), statusInfo.getDescription(),
                        jiraIssueIconPath, getCategory(statusInfo.getStatusCategoryId()));
                addStatusToStepDescriptor(sd, status);
                statuses.add(status);
            }
            isExist = false;
            setResolutionField(sd, defaultResolution.getId());
        }
        return statuses;
    }*/

}
